package com.atguigu.gulimall.member.feign;

import com.atguigu.common.utils.R;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

@Component
@FeignClient(name = "gulimall-coupon")
public interface CouponFeignService {

    @RequestMapping("/coupon/coupon/member/list")
     R memberCoupons();

}
