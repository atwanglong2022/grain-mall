package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author longlon
 * @email 3404424145@qq.com
 * @date 2022-07-23 17:40:01
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
