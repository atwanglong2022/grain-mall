package com.atguigu.gulimall.thirdparty.service;

import org.springframework.web.multipart.MultipartFile;

public interface FileOssService {
    /**
     *  // 文件上传功能
     * @param file
     * @return
     */
    String fileUpload(MultipartFile file);
}
