package com.atguigu.gulimall.thirdparty.controller;

import com.atguigu.common.utils.R;
import com.atguigu.gulimall.thirdparty.service.FileOssService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Api(tags = "文件系统服务")
@Slf4j
@RestController
@RequestMapping("/oss")
public class ApiFileOssController {
    @Autowired
    private FileOssService fileOssService;

    @ApiOperation("文件上传")
    @PostMapping("/fileUpload")
    public R fileUpload(MultipartFile file) {

        String url = fileOssService.fileUpload(file);
        System.out.println("文件上传 url = " + url);

        return R.ok(url);
    }

}
