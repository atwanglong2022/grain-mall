package com.atguigu.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.product.entity.CategoryEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author longlon
 * @email 3404424145@qq.com
 * @date 2022-07-23 14:28:50
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 商品三级分类列表用树树形结构封装返回
     *
     * @return
     */
    List<CategoryEntity> listWithTree();

    Boolean removeCategoryByIds(List<Long> catIds);

    /**
     * 找到catelogId的完整路径
     *  [父/子/孙]  完成路径,[2, 34, 225]
     * @param catelogId
     * @return
     */
    Long[] findCatelogPath(Long catelogId);

    //修改 更新数据,并级联更新
    void updateCascade(CategoryEntity category);
}

