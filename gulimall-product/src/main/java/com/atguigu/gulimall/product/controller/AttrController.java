package com.atguigu.gulimall.product.controller;

import java.util.Arrays;
import java.util.Map;

import com.atguigu.common.constant.ProductConstant;
import com.atguigu.gulimall.product.vo.AttrRespVo;
import com.atguigu.gulimall.product.vo.AttrVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.atguigu.gulimall.product.entity.AttrEntity;
import com.atguigu.gulimall.product.service.AttrService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.R;



/**
 * 商品属性
 *
 * @author longlon
 * @email 3404424145@qq.com
 * @date 2022-07-23 14:28:50
 */
@RestController
@RequestMapping("product/attr")
public class AttrController {
    @Autowired
    private AttrService attrService;

    /**
     * 2、获取分类规格参数
     * 1、URL：/product/attr/base/list/{catelogId}
     * @param params
     * @param catelogId
     * @return
     */
    @GetMapping("/base/list/{catelogId}")
    public R baseAttrList(@RequestParam Map<String,Object> params,@PathVariable("catelogId") Long catelogId){
        PageUtils page = attrService.queryBaseListAttrPage(params,catelogId);
        return R.ok().put("page", page);
    }

    /**
     * 平台属性 可以通过在添加路径变量{attrType}同时用一个方法查询销售属性和规格参数
     *      * 注意：销售属性，没有分组信息，所以复用方法的时候，要判断是销售属性还是规格参数
     * @param params
     * @param attrType
     * @param catelogId
     * @return
     */
    @RequestMapping("/{attrType}/list/{catelogId}")
    public R baseList(@RequestParam Map<String,Object> params,
                      @PathVariable("attrType")String attrType,
                      @PathVariable("catelogId") Long catelogId){

        PageUtils page = attrService.queryBaseAttrPage(params,catelogId,attrType);

        return R.ok().put("page", page);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
  //   @RequiresPermissions("product:attr:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = attrService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attrId}")
   // @RequiresPermissions("product:attr:info")
    public R info(@PathVariable("attrId") Long attrId){

		//AttrEntity attr = attrService.getById(attrId);
        AttrRespVo attrRespVo = attrService.getAttrInfo(attrId);
        return R.ok().put("attr", attrRespVo);
    }

    /**
     * 保存  原方法进行修改还要修改关联关系表 pms_attr_attrgroup_relation
     */
    @RequestMapping("/save")
   // @RequiresPermissions("product:attr:save")
    //  public R save(@RequestBody AttrEntity attr)
    public R save(@RequestBody AttrVo attrVo){
		attrService.saveAttrEntity(attrVo);
        return R.ok();
    }

    /**
     * 修改 包含关联表也要进行修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("product:attr:update")
    //  public R update(@RequestBody AttrEntity attr)
    public R update(@RequestBody AttrVo attrVo){

		//attrService.updateById(attr);
		attrService.updateAttr(attrVo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("product:attr:delete")
    public R delete(@RequestBody Long[] attrIds){
		attrService.removeByIds(Arrays.asList(attrIds));

        return R.ok();
    }

}
