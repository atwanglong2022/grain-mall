package com.atguigu.gulimall.product.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
@RequestMapping("/sku")
public class TestWebController {

    @GetMapping("test1")
    public String test(Map map) {
        System.out.println("map.getClass().getName() = " + map.getClass().getName());
        return "detail/index.html";
    }

   @GetMapping("test2")
    public String test2(Model model) {
        System.out.println("Model.getClass().getName() = " + model.getClass().getName());
        return "detail/index.html";
    }

   @GetMapping("test3")
    public String test3(ModelAndView modelAndView) {
        System.out.println("ModelAndView.getClass().getName() = " + modelAndView.getClass().getName());
        return "detail/index.html";
    }

   @GetMapping("test4")
    public String test4(ModelMap modelMap) {
        System.out.println("modelMap.getClass().getName() = " + modelMap.getClass().getName());
        return "detail/index.html";
    }


}
