package com.atguigu.gulimall.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.atguigu.gulimall.product.entity.AttrEntity;
import com.atguigu.gulimall.product.service.AttrService;
import com.atguigu.gulimall.product.service.CategoryService;
import com.atguigu.gulimall.product.vo.AttrAttrgroupRelationVo;
import com.atguigu.gulimall.product.vo.AttrVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.atguigu.gulimall.product.entity.AttrGroupEntity;
import com.atguigu.gulimall.product.service.AttrGroupService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.R;



/**
 * 属性分组
 *
 * @author longlon
 * @email 3404424145@qq.com
 * @date 2022-07-23 14:28:50
 */
@RestController
@RequestMapping("product/attrgroup")
public class AttrGroupController {
    @Autowired
    private AttrGroupService attrGroupService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private AttrService attrService;

    /**
     * 删除属性与分组的关联关系
     *   请求参数: [{"attrId":1,"attrGroupId":2}]
     * 1、url12：/product/attrgroup/attr/relation/delete
     * @return
     */
    @PostMapping("/attr/relation/delete")
    public R attrAttrRelationDelete(@RequestBody List<AttrAttrgroupRelationVo> attrgroupRelationVos ){
        attrGroupService.removeAttrAttrgroupRelation(attrgroupRelationVos);
        return R.ok();
    }

    /**
     * 获取属性分组的关联的所有属性
     * 1、URL10：/product/attrgroup/{attrgroupId}/attr/relation
     * @return
     */
    @GetMapping("/{attrgroupId}/attr/relation")
    public R getAttrRelation(@PathVariable("attrgroupId") Long attrgroupId){
      List<AttrEntity> data = attrService.getRelationAttr(attrgroupId);
        return R.ok().put("data",data);
    }

    /**
     * 列表 attrGroupService 请求 URL: http://localhost:88/api/product/attrgroup/list/0?t=1659133995008&page=1&limit=10&key=
     */
    @RequestMapping("/list/{catelogId}")
    //   @RequiresPermissions("product:attrattrgrouprelation:list")
    public R list(@RequestParam Map<String, Object> params,
                  @PathVariable("catelogId")Long catelogId){
        PageUtils page = attrGroupService.queryPageBycatelogId(params,catelogId);
        return R.ok().put("page", page);
    }



    /**
     * 列表
     */
    @RequestMapping("/list")
  //   @RequiresPermissions("product:attrgroup:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = attrGroupService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attrGroupId}")
   // @RequiresPermissions("product:attrgroup:info")
    public R info(@PathVariable("attrGroupId") Long attrGroupId){
		AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);

        Long catelogId = attrGroup.getCatelogId();
        Long[] path = categoryService.findCatelogPath(catelogId);
        attrGroup.setCatelogPath(path);

        return R.ok().put("attrGroup", attrGroup);
    }

    /**
     * 保存  原方法进行修改还要修改关联关系表 pms_attr_attrgroup_relation
     */
    @RequestMapping("/save")
   // @RequiresPermissions("product:attrgroup:save")
    // public R save(@RequestBody AttrGroupEntity attrGroup)
    public R save(@RequestBody AttrGroupEntity attrGroup){
		//attrGroupService.save(attrGroup);
		attrGroupService.save(attrGroup);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("product:attrgroup:update")
    public R update(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.updateById(attrGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("product:attrgroup:delete")
    public R delete(@RequestBody Long[] attrGroupIds){
		attrGroupService.removeByIds(Arrays.asList(attrGroupIds));

        return R.ok();
    }

}
