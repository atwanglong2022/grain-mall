package com.atguigu.gulimall.product.vo;

import lombok.Data;

@Data
public class AttrRespVo extends AttrVo {
    /**
     * "catelogName": "手机/数码/手机"
     * "groupName": "主体"
     */
    private String catelogName; // 所属分类的名字
    private String groupName; // 所属分组的名字

    private Long[] catelogPath;

}
