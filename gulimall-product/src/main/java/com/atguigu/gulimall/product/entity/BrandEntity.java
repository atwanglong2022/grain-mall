package com.atguigu.gulimall.product.entity;

import com.atguigu.common.valid.AddGroup;
import com.atguigu.common.valid.ListValue;
import com.atguigu.common.valid.UpdateGroup;
import com.atguigu.common.valid.UpdateStatusGroup;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;

/**
 * 品牌
 *
 * @author longlon
 * @email 3404424145@qq.com
 * @date 2022-07-23 14:28:50
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 品牌id
	 */
//	@Null(groups = {AddGroup.class})
//	@NotNull(groups = {UpdateGroup.class,UpdateStatusGroup.class})
	@TableId
	private Long brandId;
	/**
	 * 品牌名
	 */
//	@NotBlank(message = "品牌名称必须提交",groups = {UpdateGroup.class, AddGroup.class})
	private String name;
	/**
	 * 品牌logo地址
	 */
//	@NotEmpty
//	@URL(message = "logo必须是一个合法的url地址",groups = {UpdateGroup.class, AddGroup.class})
	private String logo;
	/**
	 * 介绍
	 */
	private String descript;
	/**
	 * 显示状态[0-不显示；1-显示]
	 */
	//@TableLogic(value = "1",delval = "0")
//	@NotNull(groups = {UpdateStatusGroup.class, AddGroup.class})
//	@ListValue(vals = {0,1},groups = {AddGroup.class, UpdateStatusGroup.class})
	private Integer showStatus;

	/**
	 * 检索首字母
	 */
//	@NotNull(message = "首字母不能为空",groups = {UpdateGroup.class, AddGroup.class})
//	@Pattern(regexp = "^[a-zA-Z]$",message = "检索的必须是一个字母")
	private String firstLetter;
	/**
	 * 排序
	 */
//	@NotEmpty
//	@Min(value = 0,message = "排序首字母大于或等于0",groups = {UpdateGroup.class, AddGroup.class})
	private Integer sort;

}
