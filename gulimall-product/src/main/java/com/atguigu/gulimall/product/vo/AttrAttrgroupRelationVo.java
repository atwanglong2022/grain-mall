package com.atguigu.gulimall.product.vo;

import com.atguigu.gulimall.product.entity.AttrAttrgroupRelationEntity;
import lombok.Data;

@Data
public class AttrAttrgroupRelationVo extends AttrAttrgroupRelationEntity {

}
