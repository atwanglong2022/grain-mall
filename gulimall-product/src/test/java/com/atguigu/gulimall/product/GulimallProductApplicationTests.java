package com.atguigu.gulimall.product;

import com.atguigu.gulimall.product.entity.BrandEntity;
import com.atguigu.gulimall.product.service.BrandService;
import com.atguigu.gulimall.product.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

@Slf4j
@SpringBootTest
public class GulimallProductApplicationTests {
    @Autowired
    private BrandService brandService;
    @Autowired
    private CategoryService categoryService;


    @Test
    public void test1CatelogPath(){
        Long[] catelogPath = categoryService.findCatelogPath(225L);
        log.info("完成路径,{}", Arrays.asList(catelogPath));

    }

    @Test
    void contextLoads() {
        BrandEntity brandEntity = brandService.getById(2L);
        System.out.println("brandEntity = " + brandEntity);
        brandEntity.setName("小米");
//        brandEntity.setDescript("非常好用啊");
//        brandEntity.setSort(1);
//        brandEntity.setLogo("www.bing.com");
        brandEntity.setFirstLetter("米");
        brandService.updateById(brandEntity);
    }

 @Test
    void contextLoads2() {

        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setName("华为");
        brandEntity.setDescript("非常好用啊");
        brandEntity.setSort(1);
        brandEntity.setLogo("www.bing.com");
        brandEntity.setFirstLetter("华");
        brandService.save(brandEntity);
    }

}
