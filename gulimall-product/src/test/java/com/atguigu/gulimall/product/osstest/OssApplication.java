package com.atguigu.gulimall.product.osstest;

import com.aliyun.oss.OSS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
@SpringBootTest
public class OssApplication {

    @Resource
    private OSS ossClient;

    @RequestMapping("/")
    public String home() throws FileNotFoundException {
        ossClient.putObject("gulimall0212", "22.jfif", new FileInputStream("D:\\桌面\\imgs\\22.jfif"));
        return "upload success";
    }

    public static void main(String[] args) throws URISyntaxException {
        SpringApplication.run(OssApplication.class, args);

    }

}
